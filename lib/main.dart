import 'package:flutter/material.dart';
import 'package:marvel_flutter/app/app.dart';
import 'package:marvel_flutter/backend/backend.dart';

void main() {
  final backend = Backend('https://gateway.marvel.com:443/v1/public');

  runApp(MarvelGuideApp(
    backend: backend,
  ));
}