import 'package:flutter/material.dart';
import 'package:marvel_flutter/app/theme.dart';
import 'package:marvel_flutter/screens/character_list/character_list_screen.dart';
import 'package:marvel_flutter/backend/backend.dart';
import 'package:provider/provider.dart';
import 'package:marvel_flutter/named_routing/router.dart' as router;
import 'package:marvel_flutter/constants.dart';

class MarvelGuideApp extends StatelessWidget {
  final Backend backend;

  MarvelGuideApp({@required this.backend}) : assert(backend != null);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Marvel Guide',
      theme: AppTheme.light(),
      darkTheme: AppTheme.dark(),
      home: Provider.value(
        value: backend,
        child: CharacterListScreen(),
      ),
      onGenerateRoute: router.generateRoute,
      initialRoute: ROUTE_INITIAL,
    );
  }
}
