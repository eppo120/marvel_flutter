import 'package:json_annotation/json_annotation.dart';

import 'comic_summary.dart';

part 'comic_list.g.dart';

@JsonSerializable()
class ComicList {
  final int available;
  final int returned;
  final String collectionURI;

  @JsonKey(name: 'items')
  final List<ComicSummary> comicSummaryList;

  ComicList(
      {this.available,
      this.returned,
      this.collectionURI,
      this.comicSummaryList});

  factory ComicList.fromJson(Map<String, dynamic> json) =>
      _$ComicListFromJson(json);

  Map<String, dynamic> toJson() => _$ComicListToJson(this);
}
