// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'comic_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ComicList _$ComicListFromJson(Map<String, dynamic> json) {
  return ComicList(
    available: json['available'] as int,
    returned: json['returned'] as int,
    collectionURI: json['collectionURI'] as String,
    comicSummaryList: json['comicSummaryList'] as List,
  );
}

Map<String, dynamic> _$ComicListToJson(ComicList instance) => <String, dynamic>{
      'available': instance.available,
      'returned': instance.returned,
      'collectionURI': instance.collectionURI,
      'comicSummaryList': instance.comicSummaryList,
    };
