// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'character_data_wrapper.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CharacterDataWrapper _$CharacterDataWrapperFromJson(Map<String, dynamic> json) {
  return CharacterDataWrapper(
    code: json['code'] as int,
    status: json['status'] as String,
    copyright: json['copyright'] as String,
    attributionText: json['attributionText'] as String,
    attributionHTML: json['attributionHTML'] as String,
    etag: json['etag'] as String,
    characterDataContainer: json['data'] == null
        ? null
        : CharacterDataContainer.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$CharacterDataWrapperToJson(
        CharacterDataWrapper instance) =>
    <String, dynamic>{
      'code': instance.code,
      'status': instance.status,
      'copyright': instance.copyright,
      'attributionText': instance.attributionText,
      'attributionHTML': instance.attributionHTML,
      'etag': instance.etag,
      'data': instance.characterDataContainer,
    };
