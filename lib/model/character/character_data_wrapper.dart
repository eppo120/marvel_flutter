import 'package:marvel_flutter/model/character/character_data_container.dart';
import 'package:json_annotation/json_annotation.dart';

part 'character_data_wrapper.g.dart';

@JsonSerializable()
class CharacterDataWrapper {
  final int code;
  final String status;
  final String copyright;
  final String attributionText;
  final String attributionHTML;
  final String etag;
  @JsonKey(name: 'data')
  final CharacterDataContainer characterDataContainer;

  CharacterDataWrapper(
      {this.code,
      this.status,
      this.copyright,
      this.attributionText,
      this.attributionHTML,
      this.etag,
      this.characterDataContainer});

  factory CharacterDataWrapper.fromJson(Map<String, dynamic> json) =>
      _$CharacterDataWrapperFromJson(json);

  Map<String, dynamic> toJson() => _$CharacterDataWrapperToJson(this);
}
