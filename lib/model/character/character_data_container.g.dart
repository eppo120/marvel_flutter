// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'character_data_container.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CharacterDataContainer _$CharacterDataContainerFromJson(
    Map<String, dynamic> json) {
  return CharacterDataContainer(
    offset: json['offset'] as int,
    limit: json['limit'] as int,
    total: json['total'] as int,
    count: json['count'] as int,
    characterList: (json['results'] as List)
        ?.map((e) =>
            e == null ? null : Character.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$CharacterDataContainerToJson(
        CharacterDataContainer instance) =>
    <String, dynamic>{
      'offset': instance.offset,
      'limit': instance.limit,
      'total': instance.total,
      'count': instance.count,
      'results': instance.characterList,
    };
