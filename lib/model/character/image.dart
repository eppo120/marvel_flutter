import 'package:json_annotation/json_annotation.dart';

part 'image.g.dart';

@JsonSerializable()
class Image {
  final String path;
  final String extension;

  Image({this.path, this.extension});

  String get fullPath {
    return this.path.replaceAll("http", "https") + "." + extension;
  }

  factory Image.fromJson(Map<String, dynamic> json) => _$ImageFromJson(json);

  Map<String, dynamic> toJson() => _$ImageToJson(this);
}