import 'package:marvel_flutter/model/character/character.dart';
import 'package:json_annotation/json_annotation.dart';

part 'character_data_container.g.dart';

@JsonSerializable()
class CharacterDataContainer {
  final int offset;
  final int limit;
  final int total;
  final int count;

  @JsonKey(name: 'results')
  final List<Character> characterList;

  CharacterDataContainer(
      {this.offset, this.limit, this.total, this.count, this.characterList});

  factory CharacterDataContainer.fromJson(Map<String, dynamic> json) =>
      _$CharacterDataContainerFromJson(json);

  Map<String, dynamic> toJson() => _$CharacterDataContainerToJson(this);
}
