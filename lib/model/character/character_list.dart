class CharacterList {
  final List<dynamic> characterList;

  CharacterList({this.characterList});

  factory CharacterList.fromJson(Map<String, dynamic> json) {
    return CharacterList(
      characterList: json['results'],
    );
  }
}