import 'package:flutter/material.dart';
import 'package:marvel_flutter/constants.dart';
import 'package:marvel_flutter/screens/character_detail/character_detail_screen.dart';
import 'package:marvel_flutter/screens/character_list/character_list_screen.dart';

Route<dynamic> generateRoute(RouteSettings settings) {

  switch(settings.name) {
    case ROUTE_INITIAL:
      return MaterialPageRoute(builder: (context) => CharacterListScreen());
    case ROUTE_CHARACTER_DETAIL:
      var character = settings.arguments;
      return MaterialPageRoute(builder: (context) => CharacterDetailScreen(character: character,));
  }

}