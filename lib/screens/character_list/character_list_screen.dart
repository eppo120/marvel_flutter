import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:marvel_flutter/backend/backend.dart';
import 'package:marvel_flutter/constants.dart';
import 'package:marvel_flutter/model/character/character.dart';
import 'package:marvel_flutter/screens/character_list/character_list_tile.dart';
import 'package:provider/provider.dart';

class CharacterListScreen extends StatefulWidget {
  @override
  _CharacterListScreenState createState() => _CharacterListScreenState();
}

class _CharacterListScreenState extends State<CharacterListScreen> {
  static const _pageSize = 20;

  final PagingController<int, Character> _pagingController =
      PagingController(firstPageKey: 0);

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
    super.initState();
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      final newCharacters =
          await context.read<Backend>().fetchCharacters(pageKey, _pageSize);
      final isLastPage = newCharacters.length < _pageSize;
      if (isLastPage) {
        _pagingController.appendLastPage(newCharacters);
      } else {
        final nextPageKey = pageKey + newCharacters.length;
        _pagingController.appendPage(newCharacters, nextPageKey);
      }
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Marvel Guide'),
        ),
        body: PagedListView<int, Character>(
          pagingController: _pagingController,
          builderDelegate: PagedChildBuilderDelegate<Character>(
            itemBuilder: (context, item, index) => CharacterListTile(
                character: item,
                onTap: () {
                  Navigator.of(context)
                      .pushNamed(ROUTE_CHARACTER_DETAIL, arguments: item);
                }),
          ),
        ));
  }
}

// FutureBuilder<CharacterDataWrapper>(
// future: context.read<Backend>().fetchCharacters(),
// builder: (context, snapshot) {
// if (snapshot.hasData) {
// var characterList =
// snapshot.data.characterDataContainer.characterList;
// return ListView(
// children: [
// for (final character in characterList)
// CharacterListTile(
// onTap: () {
// Navigator.of(context).pushNamed(ROUTE_CHARACTER_DETAIL,
// arguments: character);
// },
// character: character)
// ],
// );
// } else if (snapshot.hasError) {
// return Text("${snapshot.error}");
// }
// return Center(
// child: CircularProgressIndicator(),
// );
// },
// ),
