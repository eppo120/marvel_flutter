import 'package:flutter/material.dart';
import 'package:marvel_flutter/model/character/character.dart';

class CharacterListTile extends StatelessWidget {
  final Character character;
  final VoidCallback onTap;

  CharacterListTile({
    Key key,
    @required this.character,
    @required this.onTap,
  })  : assert(character != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      isThreeLine: true,
      leading: AspectRatio(
          aspectRatio: 1,
          child: Hero(
            tag: 'hero-${character.id}-image',
            child: Image.network(
              character.thumbnail.fullPath,
              fit: BoxFit.cover,
            ),
          )),
      onTap: onTap,
      title: Text(character.name),
      subtitle: Text(
        character.description,
        maxLines: 2,
        overflow: TextOverflow.ellipsis,
      ),
      trailing: Icon(Icons.chevron_right),
    );
  }
}
