import 'package:flutter/material.dart';
import 'package:marvel_flutter/model/character/character.dart';

class CharacterDetailScreen extends StatelessWidget {
  final Character character;

  CharacterDetailScreen({Key key, @required this.character})
      : assert(character != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(character.name),
      ),
      body: ListView(
        children: [
          _ImageHeader(character: character),
          Text(
            character.name,
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
          Text(character.description),
        ],
      ),
    );
  }
}

class _ImageHeader extends StatelessWidget {
  const _ImageHeader({
    Key key,
    @required this.character,
  }) : super(key: key);

  final Character character;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 250,
      child: Hero(
        tag: 'hero-${character.id}-image',
        child: Image.network(
          character.thumbnail.fullPath,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
