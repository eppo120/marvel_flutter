import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:http/http.dart' as http;
import 'package:marvel_flutter/model/character/character.dart';
import 'package:marvel_flutter/model/character/character_data_wrapper.dart';
import 'package:marvel_flutter/keys.dart';

class Backend {
  final String hostUrl;

  const Backend(this.hostUrl);

  Future<List<Character>> fetchCharacters(int offset, int limit) async {
    //https://gateway.marvel.com:443/v1/public
    var publickey =
        publicKey; //Put your Marvel public key here (I have it in a seperate keys file)
    var privatekey = privateKey; //Put your Marvel Private key here
    var ts = DateTime.now().toString();
    var stringToHash = ts + privatekey + publickey;
    var hash = md5.convert(utf8.encode(stringToHash)).toString();
    var baseUrl = '$hostUrl/characters';
    var url = baseUrl +
        '?offset=' +
        '$offset' +
        '?limit=' +
        '$limit' +
        '&ts=' +
        ts +
        '&apikey=' +
        publickey +
        '&hash=' +
        hash;

    final response = await http.get(url);

    if (response.statusCode == 200) {
      return CharacterDataWrapper.fromJson(jsonDecode(response.body))
          .characterDataContainer
          .characterList;
    } else {
      throw Exception('Failed to load characters');
    }
  }
}
